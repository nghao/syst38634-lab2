package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTime {

	@Test
	public void testTotalMillisecondsRegular() {
		int totalSeconds = Time.getTotalMilliseconds("12:05:05:05");
		assertTrue(totalSeconds == 05);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testTotalMillisecondsException() {
		int totalSeconds = Time.getTotalMilliseconds("12:05:05:--");
		fail("The time provided is not valid");
	}

}
